// Search for a Star 2023

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DefaultPawn.h"
#include "STBPawn.generated.h"

UCLASS()
class STB_API ASTBPawn : public APawn
{
	GENERATED_BODY()

public:
	ASTBPawn();

private:
	UPROPERTY()
	class UCapsuleComponent* CapsuleCollider;
	
	UPROPERTY(VisibleAnywhere, Category="Components")
	class USpringArmComponent* SpringArm;
	
	UPROPERTY(VisibleAnywhere, Category="Components")
	class UCameraComponent* Camera;

public:
	// Called to bind functionality to input. 
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;
};
